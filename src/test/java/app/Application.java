package app;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;


import java.util.concurrent.TimeUnit;

public class Application {

    private static WebDriver driver;

    public Application() {
        driver = new EventFiringWebDriver(getDriver());
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    public void quit() {
        driver.quit();
        driver = null;
    }

    public WebDriver getDriver() {
        String browserName = "chrome";
        return BrowsersFactory.buildDriver(browserName);
    }

}
