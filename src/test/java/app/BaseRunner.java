package app;

import org.junit.Before;

public class BaseRunner {

    private static ThreadLocal<Application> threadApp = new ThreadLocal<>();
    protected Application app;

    @Before
    public void start() {
        if (threadApp.get() != null) {
            app = threadApp.get();
            return;
        }
        app = new Application();
        threadApp.set(app);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            app.quit();
        }));
    }

}
