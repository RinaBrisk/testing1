package tests;

import app.BaseRunner;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class WebsiteTests extends BaseRunner {

    @Test
    public void OpenAndCloseSites() throws URISyntaxException, IOException {
        URI websitesUrl = Objects.requireNonNull(this.getClass().getClassLoader().getResource("Websites.txt")).toURI();
        List<String> websitesInAnotherOnder = new ArrayList<>();

        Files.lines(Paths.get(websitesUrl), StandardCharsets.UTF_8).forEach(site ->
        {
            app.getDriver().navigate().to(site);
            websitesInAnotherOnder.add(site);
        });
        Files.delete(Paths.get(websitesUrl));
        Files.write(Paths.get(websitesUrl), websitesInAnotherOnder);
    }

}
